import 'package:flutter/material.dart';
import 'package:proy_floreshnos/pages/welcome/welcome_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, //borrar el banner de DEBUG
      title: 'Encomiendas App - Flores hnos.',
      home: WelcomePage(),
    );
  }
}
