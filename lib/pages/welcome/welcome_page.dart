import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:proy_floreshnos/pages/registro/registro_page.dart';
import 'package:proy_floreshnos/pages/subscribirse/subscribirse_page.dart';
import 'package:proy_floreshnos/pages/welcome/snippets/background_welcome.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:proy_floreshnos/snippets/boton_rojo_borde.dart';
import 'package:proy_floreshnos/strings.dart'; //Google Fonts

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
        //lo colocamos en un safe Area
        child: BackGroundWelcome(
          wdget: SingleChildScrollView(
            //para poder hacer el scroll
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width:
                      size.height * 1.2, //el ancho segun el tamaño del celular
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Image.asset('assets/images/jpg/floreshnos_img.jpg'),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 50),
                  child: Text(
                    'Registra los datos de tu envio y evita el contacto con las personas',
                    softWrap: true,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.raleway(
                      fontSize: 12,
                      color: Colors.grey[800],
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  height: size.height * 0.2,
                ),
                BotonRojoBordes(
                  // ** tenemos que importar
                  //creamos un widget como snippet
                  text: 'REGISTRAR',
                  press: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return RegistroPage();
                    }));
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context) {
                          return SubscribirsePage();
                        },
                      ));
                    },
                    child: new Text(
                      "Subscribirse",
                      style: GoogleFonts.raleway(
                          fontSize: 12,
                          decoration: TextDecoration
                              .underline), //configuramos el tamaño y el subrayado
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
