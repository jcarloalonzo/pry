import 'package:flutter/material.dart';

class BackGroundWelcome extends StatelessWidget {
  final Widget wdget;

  const BackGroundWelcome({
    Key key,
    this.wdget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      //para respetar el contenido
      child: Container(
        height: size.height, //indicamos que usaremos todo el alto del telefono
        width: size.width, //indicamos que usaremos todo el ancho del telefono
        color: Colors.white,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
              bottom: 0,
              left: 0,
              height: size.height * 0.06,
              // child: Image.asset('assets/images/png/jelaf_logo.png'),
              child: Container(
                padding: EdgeInsets.all(04),
                decoration: BoxDecoration(
                  color: Colors.grey[800],
                  borderRadius: BorderRadius.circular(07),
                ),
                child: Image.asset('assets/images/png/jelaf_logo.png'),
              ),
            ),
            wdget,
          ],
        ),
      ),
    );
  }
}
