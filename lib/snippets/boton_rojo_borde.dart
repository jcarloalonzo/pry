import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:proy_floreshnos/strings.dart';

class BotonRojoBordes extends StatelessWidget {
  final Color color, textColor;
  final String text;
  final Function press;
  const BotonRojoBordes({
    Key key,
    this.color = cRojo,
    this.textColor = Colors.white,
    this.text,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      margin: EdgeInsets.only(top: 10),
      width: size.width * 0.40,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(
            15), //configurando el radio del boton registrars
        child: FlatButton(
          color: cRojo, //color rojo establecido en el archivo strings.dart
          padding: EdgeInsets.symmetric(horizontal: 09, vertical: 14),
          onPressed:
              //    Navigator.push(context, MaterialPageRoute(
              //      builder: (context) {
              //       return RegistroPage();
              //     },
              //   ));
              press,

          child: Text(
            text,
            style: GoogleFonts.raleway(
              fontSize: 12,
              color: textColor,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }
}
